#!/usr/bin/env bash

MAX_PROCESSES=8
LABEL=''

usage() {
    local message=$1
    local sev=$2
    # TODO: need to add some man and help information
    case "$sev" in
        fatal|error)
            sev='error'
            ;;
        *)
            sev='info'
            ;;
    esac
    log "$message" $sev
    case "$sev" in
        error|critical)
            finish 255 "Can't go on" $sev
            ;;
        *)
            finish 255 '' 'info'
            ;;
    esac
}

parse_args() {
    local arg
    LOG_LEVEL=warning
    for arg in "$@"; do
        case $arg in
            -l=*|--log-file=*)
                LOG_FILE="${arg#*=}"
                ;;
            -b=*|--label=*)
                LABEL="${arg#*=}"
                ;;
            -q=*|--work-queue=*)
                local work_queue="${arg#*=}"
                { # try
                    [ -z "$work_queue" ] && usage "work-queue is blank?!?" "fatal"
                    exec 3< $work_queue &&
                    readarray -u 3 PROCESS_LIST &&
                    exec 3>&-
                } || { # except
                    finish 255 "Can't open $work_queue to read: $?" "fatal"
                }
                ;;
            -p=*|--process-max=*)
                MAX_PROCESSES="${arg#*=}"
                { # try
                    ((MAX_PROCESSES+=0))
                } || { # except
                    finish 255 "Invalid --process-max: ${MAX_PROCESSES}" "fatal"
                }
                ;;
            -d|--debug)
                LOG_LEVEL=debug
                ;;
            -l=*|--log-level=*)
                local log_test="${arg#*=}"
                : $(_rank_sev ${log_test}) || usage "Invalid log level: ${log_test}" "fatal"
                LOG_LEVEL=$log_test
                ;;
            *)
                usage "I don't know what to do with arg $arg" "fatal"
        esac
    done
}

_rank_sev() {
    local sev_level
    case $1 in
        debug)
            sev_level=4
            ;;
        info)
            sev_level=3
            ;;
        warning|warn)
            sev_level=2
            ;;
        error)
            sev_level=1
            ;;
        critical)
            sev_level=0
            ;;
        *)
            echo "Invalid log level: $1"
            return -1
            ;;
    esac
    echo $sev_level
}


log() {
    local message=" - $1"
    local severity=${2:-info}
    local log_level_n=$(_rank_sev $LOG_LEVEL)
    local sev_level_n=$(_rank_sev $severity)
    local label=''
    [ -n "$LABEL" ] && label=" - ${LABEL:0:9}"
    if [ $sev_level_n -le $log_level_n ]; then
        echo "$(date) - ${severity}${label}$message" >&2
    fi
}

finish() {
    local rc="${1:-0}"
    local message
    local sev
    LOG_LEVEL='info'
    if [ -n "$2" ]; then
        message="$2"
    fi
    if [ $rc == "0" ]; then
        sev="info"
    else
        sev="error"
    fi
    log "COMPLETE: (${rc}) $message" $sev
    _remove_lock
    trap '' EXIT
    exit $rc
}

LCK_FILE="/tmp/multi_bash"
LCK_FD=99
LOCKED=no

_remove_lock() {
    [ $LOCKED == 'no' ] && return 1
    flock -u "$LCK_FD" 2>/dev/null
    rm -f "$LCK_FILE.lock"
}

_create_lock() {
    eval "exec $LCK_FD>>\"$LCK_FILE.lock\""
    trap "finish; exit 0" EXIT
    trap 'finish $?' HUP INT QUIT TRAP TERM

    flock -n $LCK_FD || { msg_already_running; exit 1; }
    echo "$$" > "$LCK_FILE.lock"
    LOCKED=yes
}

msg_already_running() {
    log "Already running." "warning"
    local pid=$(cat "$LCK_FILE.lock")
    if [ -z "$pid" ]; then
        log "No PID was found in the lock file. Verify no $0 processes are running and try again." "info"
    else
        log "PID $pid was the last process to hold the lock. If it's not running, you can safely try again." "info"
    fi
}

wait_any() {
    local rv
    while [ ${#PID_LIST[@]} -gt 0 ]; do
        local debug_message
        log "Waiting. There are ${#PID_LIST[@]} in the reactor." "debug"
        for i in $(seq 0 1 $((${#PID_LIST[@]} - 1 ))); do
            debug_message="PID_LIST[$i]: ${PID_LIST[$i]} out of ${#PID_LIST[@]}"
            log "$debug_message" "debug"
            pid=${PID_LIST[$i]}
            kill -0 $pid 2>/dev/null
            if [ $? != 0 ]; then
                log "$pid is ready. wait for it:" "debug"
                wait $pid
                rv=$?
                log "wait $pid returned $rv" "debug"
                new_array=()
                unset 'PID_LIST[i]'
                for e in "${PID_LIST[@]}"; do
                    new_array+=($e)
                done
                PID_LIST=("${new_array[@]}")
                return 0
            else
                log "$pid still running" "debug"
            fi
        done
        sleep 1
    done
    log "Wait returning ${#PID_LIST[@]}" "debug"
    [ ${#PID_LIST[@]} == 0 ] && return 1
    return 0
}

enqueue() {
    local job="$1"
    shift
    if [ ${#PID_LIST[@]} -ge $MAX_PROCESSES ]; then
        log "Waiting (${#PID_LIST[@]} in reactor)."
        wait_any
    fi
    log "enqueue $job"
    local message="Arguments: $@"
    log "$message" "debug"
    $job "$@" &
    PID_LIST+=($!)
    log "enqueued $job PID is $!" "debug"
}

reactor() {
    local x
    local count=${#PROCESS_LIST[@]}
    log "There are $MAX_PROCESSES available slots in the reactor and" "debug"
    log "There are $count jobs in our queue... let's get busy:" "debug"
    for x in "${PROCESS_LIST[@]}"; do
        enqueue $x
        ((count-=1))
        log "There are $count jobs remaining in the queue." "debug"
    done

    log "Queue is empty. ${#PID_LIST[@]} remaining in reactor."
    while wait_any; do
        log "Waiting. There are ${#PID_LIST[@]} in the reactor." "debug"
        log "Waiting for jobs to finish: ${#PID_LIST[@]} remaining."
        sleep 2
    done
}


parse_args "$@"

: ${LOG_LEVEL:='info'}
if [ -n "$LOG_FILE" ]; then
    LOG_DIR=$(dirname $LOG_FILE)/
    LOG_FILE=$(basename $LOG_FILE)
else
    : ${LOG_DIR:='/var/log/'}
    LOG_FILE=$(basename $0 --suffix=.sh)
fi
if [ ! -d $LOG_DIR ]; then
    mkdir -p $LOG_DIR || finish $? "Unable to create log dirctory: $LOG_DIR $!"
fi

_create_lock

exec >> ${LOG_DIR}/${LOG_FILE} 2>&1 || finish $? "Unable to create log file: $LOGDIR/$LOG_FILE $!"

log "START:"
PID_LIST=()
reactor
